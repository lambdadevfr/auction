#!/bin/bash
#set -x

# Get path of the current script dir
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
WRK=$DIR/wrk
# Cleaning
rm -fr $WRK/*.json

mkdir -p $WRK


SERVER=localhost:8088
AUCTIONHOUSE=http://$SERVER/auctionHouse
CURL="curl -s --header \"Content-Type: application/json\""

echo
echo "1. Create an auction."
Request1="$CURL $AUCTIONHOUSE/auction --data @$DIR/01-create-auction.json"
ID=`eval $(echo $Request1)`
echo "Request: $Request1"
echo "Response: auction created: $ID"

echo
echo "2. Get the auction."
Request2="$CURL -X GET $AUCTIONHOUSE/auction/$ID"
Auction=`eval $(echo $Request1)`
echo "Request: $Request2"
echo "Response: Auction $ID: $Auction"

echo 
echo "3. Prepare the update of the planned auction"
sed -e "s/\$ID/$ID/g" $DIR/03-update-auction.json > $WRK/03-update-auction.json
Request3="$CURL -X PUT $AUCTIONHOUSE/auction --data @$WRK/03-update-auction.json"
Update_result=`eval $(echo $Request3)`
echo "Request: $Request3"
echo "Response: update auction $ID: $Update_result"

echo 
echo "4. Get the auction $ID again - Should have changed."
Request4="$CURL -X GET $AUCTIONHOUSE/auction/$ID"
Auction=`eval $(echo $Request4)`
echo "Request: $Request4"
echo "Response: $Auction"

echo 
echo "5. Prepare the update of the opened auction - should not work."
sed -e "s/\$ID/$ID/g" $DIR/05-update-auction-forbidden.json > $WRK/05-update-auction-forbidden.json
Request5="$CURL -X PUT $AUCTIONHOUSE/auction --data @$WRK/05-update-auction-forbidden.json"
Update_result_forbidden=`eval $(echo $Request5)`
echo "Request: $Request5"
echo "Response: $Update_result_forbidden"

echo 
echo "6. Get the auction $ID again - Should not have changed."
Request6="$CURL -X GET $AUCTIONHOUSE/auction/$ID"
Auction2=`eval $(echo $Request6)`
echo "Request: $Request6"
echo "Response: $Auction2"

echo
echo "7. Add a bidder: Gérard."
sed -e "s/\$ID/$ID/g" $DIR/07-add-bidder1.json > $WRK/07-add-bidder1.json
Request7="$CURL $AUCTIONHOUSE/auction/$ID/bidder --data @$WRK/07-add-bidder1.json"
Bidder_id1=`eval $(echo $Request7)`
echo "Request: $Request7"
echo "Response: id: $Bidder_id1"

echo
echo "8. Add a bidder: René."
sed -e "s/\$ID/$ID/g" $DIR/08-add-bidder2.json > $WRK/08-add-bidder2.json
Request8="$CURL $AUCTIONHOUSE/auction/$ID/bidder --data @$WRK/08-add-bidder2.json"
Bidder_id2=`eval $(echo $Request8)`
echo "Request: $Request8"
echo "Response: id: $Bidder_id2"

echo
echo "9. Get the auction $ID again - Should have 2 more bidders."
Request9="$CURL -X GET $AUCTIONHOUSE/auction/$ID"
Auction3=`eval $(echo $Request9)`
echo "Request: $Request9"
echo "Response: $Auction3"

echo
echo "10. Add a bid < initial price for Bidder 1 (id:$Bidder_id1)."
Request10="$CURL $AUCTIONHOUSE/auction/$ID/bidder/$Bidder_id1/bid -d '{\"value\": 90}'"
Result10=`eval $(echo $Request10)`
echo "Request: $Request10"
echo "Response: $Result10"

echo
echo "11. Add a bid > initial price but < constant for Bidder 1 (id:$Bidder_id1)."
Request11="$CURL $AUCTIONHOUSE/auction/$ID/bidder/$Bidder_id1/bid -d '{\"value\": 110}'"
Result11=`eval $(echo $Request11)`
echo "Request: $Request11"
echo "Response: $Result11"

echo
echo "12. Add a valid bid for Bidder 1 (id: $Bidder_id1)."
Request12="$CURL $AUCTIONHOUSE/auction/$ID/bidder/$Bidder_id1/bid -d '{\"value\": 120}'"
Result12=`eval $(echo $Request12)`
echo "Request: $Request12"
echo "Response: $Result12"

echo
echo "13. Add a valid bid for Bidder 2 (id: $Bidder_id2)."
Request13="$CURL $AUCTIONHOUSE/auction/$ID/bidder/$Bidder_id2/bid -d '{\"value\": 150}'"
Result13=`eval $(echo $Request13)`
echo "Request: $Request13"
echo "Response: $Result13"

echo
echo "20. Get the auction."
Request20="$CURL -X GET $AUCTIONHOUSE/auction/$ID"
Auction20=`eval $(echo $Request20)`
echo "Request: $Request20"
echo "Response: $Auction20"

# Cleaning
rm -fr $WRK/*.json

echo
echo "End."
