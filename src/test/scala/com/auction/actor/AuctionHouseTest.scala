package com.auction.actor

import java.time.ZonedDateTime

import com.auction.model._
import com.auction.{PublicId, timeout}
import org.scalatest.FlatSpec

import scala.concurrent.Await

class AuctionHouseTest extends FlatSpec {
  val auctioneer = Auctioneer("Annah")

  val initialPrice = Price(100)
  val policy = ConstantIncrementPolicy(Price(10))
  val auctionPlanned = Auction(Item("one"), initialPrice, ZonedDateTime.parse("2019-10-25T00:00:01Z"), ZonedDateTime.parse("2019-12-25T00:00:01Z"), policy)
  val auctionOpened = Auction(Item("two"), initialPrice, ZonedDateTime.parse("2018-03-25T00:00:01Z"), ZonedDateTime.parse("2019-12-25T00:00:01Z"), policy)
  val auctionClosed = Auction(Item("tree"), initialPrice, ZonedDateTime.parse("2018-02-10T00:00:01Z"), ZonedDateTime.parse("2018-12-20T00:00:01Z"), policy)

  it should "check auctions used in fixtures" in {
    val now = ZonedDateTime.now()
    assert(auctionOpened.beginAt.isBefore(now))
    assert(auctionOpened.endAt.isAfter(now))

    assert(auctionPlanned.beginAt.isAfter(now))
    assert(auctionPlanned.endAt.isAfter(now))

    assert(auctionClosed.beginAt.isBefore(now))
    assert(auctionClosed.endAt.isBefore(now))
  }

  "An AuctionHouse" should "be able to retrieve an Auction By Id" in {
    val id: PublicId = addAuction(auctionPlanned)

    val auctionsBefore = getAuctionResponseMessage(id)
    assert(auctionsBefore.presentAuctionValues.auctionDefinition == auctionPlanned)
    assert(auctionsBefore.presentAuctionValues.id == id)
    assert(auctionsBefore.presentAuctionValues.status == AuctionStatus.Planned)
    assert(auctionsBefore.presentAuctionValues.bids == List.empty[Bid])
  }

  "An AuctionHouse" should "be able to update a planned Auction" in {
    val id = addAuction(auctionPlanned)
    val auctionBefore = getAuctionResponseMessage(id)
    assert(auctionBefore.presentAuctionValues.auctionDefinition == auctionPlanned)
    assert(auctionBefore.presentAuctionValues.status == AuctionStatus.Planned)

    updateAuction(auctioneer, id, auctionOpened)
    val auctionAfter = getAuctionResponseMessage(id)
    assert(auctionAfter.presentAuctionValues.status == AuctionStatus.Opened)
    assert(auctionAfter.presentAuctionValues.auctionDefinition == auctionOpened)
  }

  "An AuctionHouse" should "be able to block Auction update if the auction is opened" in {
    val id = addAuction(auctionOpened)
    val auctionBefore = getAuctionResponseMessage(id)
    assert(auctionBefore.presentAuctionValues.status == AuctionStatus.Opened)
    assert(auctionBefore.presentAuctionValues.auctionDefinition == auctionOpened)

    updateAuction(auctioneer, id, auctionPlanned)
    val auctionAfter = getAuctionResponseMessage(id)
    assert(auctionAfter.presentAuctionValues.status == AuctionStatus.Opened)
    assert(auctionBefore == auctionAfter)
  }

  "An AuctionHouse" should "be able to block Auction update if the auction is closed" in {
    val id = addAuction(auctionClosed)
    val auctionBefore = getAuctionClosedResponseMessage(id)
    assert(auctionBefore.closedAuctionDto.auctionDefinition == auctionClosed)

    updateAuction(auctioneer, id, auctionPlanned)
    val auctionAfter = getAuctionClosedResponseMessage(id)
    assert(auctionAfter == auctionAfter)
  }


  private def addAuction(auction: Auction): PublicId = {
    val future = AuctionHouse.addAuction(auctioneer, auction)
    Await.result(future, timeout.duration).id
  }

  private def getAuctionResponseMessage(id: PublicId) = {
    val future = AuctionHouse.auction(id)
    Await.result(future, timeout.duration).asInstanceOf[GetAuctionResponseMessage]
  }

  private def getAuctionClosedResponseMessage(id: PublicId) = {
    val future = AuctionHouse.auction(id)
    Await.result(future, timeout.duration).asInstanceOf[GetAuctionClosedResponseMessage]
  }

  private def updateAuction(auctioneer: Auctioneer, id: PublicId, auction: Auction) = {
    val future = AuctionHouse.updateAuction(auctioneer, id, auction)
    Await.result(future, timeout.duration)
  }

}
