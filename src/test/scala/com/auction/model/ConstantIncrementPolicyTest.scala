package com.auction.model

import org.scalatest._

class ConstantIncrementPolicyTest extends FlatSpec {
  "A ConstantIncrementPolicy" should "check if a Price is valid if greater than price+constant" in {
    testConstantValidity(List(200, 2000001, 110), true)
  }

  "A ConstantIncrementPolicy" should "check if a Price is not valid if lower than price+constant" in {
    testConstantValidity(List(99, 100, 109), false)
  }

  private val policy = ConstantIncrementPolicy(Price(10))
  private val initialPrice = Price(100)

  private def testConstantValidity(testValues: List[Long], expected: Boolean) =
    testValues.foreach { value =>
      val newPrice = Price(value)
      assert(policy.isBidValid(initialPrice, newPrice) == expected)
    }

  it should "throw IllegalArgumentException if Constant is negative or is equal 0" in {
    assertThrows[IllegalArgumentException] {
      ConstantIncrementPolicy(Price(0))
    }
    assertThrows[IllegalArgumentException] {
      ConstantIncrementPolicy(Price(-10))
    }
  }
}
