package com.auction.actor

import java.time.format.DateTimeFormatter
import java.time.{ZoneId, ZonedDateTime}

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.auction.dto.CreateAuctionDto
import com.auction.model._
import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat}

package object http {

  trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

    // Taken from https://github.com/xebialabs-community/xl-release-stress-tests/blob/master/data-generator/src/main/scala/com/xebialabs/xlrelease/json/ZonedDateTimeProtocol.scala
    implicit object ZonedDateTimeProtocol extends RootJsonFormat[ZonedDateTime] {

      private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault)

      def write(obj: ZonedDateTime): JsValue = {
        JsString(formatter.format(obj))
      }

      def read(json: JsValue): ZonedDateTime = json match {
        case JsString(s) => try {
          ZonedDateTime.parse(s, formatter)
        } catch {
          case t: Throwable => error(s)
        }
        case _ =>
          error(json.toString())
      }

      def error(v: Any): ZonedDateTime = {
        val example = formatter.format(ZonedDateTime.now())
        throw new IllegalArgumentException(f"'$v' is not a valid date value. Dates must be in compact ISO-8601 format, e.g. '$example'")
      }
    }


    implicit val priceFormat = jsonFormat1(Price)
    implicit val itemFormat = jsonFormat1(Item)
    implicit val auctioneerFormat = jsonFormat1(Auctioneer)
    implicit val constantIncrementPolicyFormat = jsonFormat1(ConstantIncrementPolicy)
    implicit val auctionFormat = jsonFormat5(Auction)
    implicit val createAuctionDtoFormat = jsonFormat2(CreateAuctionDto)
  }

}
