package com.auction.http

import java.util.UUID

import com.auction.PublicId
import spray.json.{JsString, JsValue, RootJsonFormat}

object PublicIdJsonConverter extends RootJsonFormat[PublicId] {
  override def write(obj: PublicId): JsValue = JsString(obj.toString)

  override def read(json: JsValue): PublicId = {
    json match {
      case JsString(s) => UUID.fromString(s)
      case _ => throw new IllegalArgumentException(s"Can not convert $json")
    }
  }

}
