package com.auction.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.auction.actor._
import com.auction.dto.{ClosedAuctionDto, CreateAuctionDto, PresentAuctionDto, UpdateAuctionDto}
import com.auction.model._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

object WebServer {
  val port = 8088
  // needed to run the route
  implicit val system = ActorSystem("my-system")


  // Taken from https://github.com/xebialabs-community/xl-release-stress-tests/blob/master/data-generator/src/main/scala/com/xebialabs/xlrelease/json/ZonedDateTimeProtocol.scala


  implicit val zdtFormat = ZonedDateTimeProtocol
  implicit val publicIdFormat = PublicIdJsonConverter
  implicit val priceFormat = jsonFormat1(Price)
  implicit val itemFormat = jsonFormat1(Item)
  implicit val auctioneerFormat = jsonFormat1(Auctioneer)
  implicit val constantIncrementPolicyFormat = jsonFormat1(ConstantIncrementPolicy)
  implicit val bidderFormat = jsonFormat1(Bidder)
  implicit val bidAcceptedFormat = jsonFormat4(BidAccepted)
  implicit val auctionFormat = jsonFormat5(Auction)
  implicit val auctionStatusFormat = new EnumJsonConverter(AuctionStatus)
  implicit val createAuctionDtoFormat = jsonFormat2(CreateAuctionDto)
  implicit val updateAuctionDtoFormat = jsonFormat3(UpdateAuctionDto)
  implicit val presentAuctionDtoFormat = jsonFormat6(PresentAuctionDto)
  implicit val closedAuctionDtoFormat = jsonFormat6(ClosedAuctionDto)

  def main(args: Array[String]) {

    val route =
      pathPrefix("auctionHouse") {
        pathPrefix("auction" / JavaUUID) { auctionId =>
          pathPrefix("bidder" / JavaUUID) { bidderId =>
            path("bid") {
              post {
                entity(as[Price]) { price =>
                  val future = AuctionHouse.addBid(auctionId, bidderId, price)
                  onSuccess(future) {
                    case AddBidResponseOKMessage() => complete(StatusCodes.OK)
                    case AddBidResponseKOMessage(message) => complete(StatusCodes.Forbidden, message)
                    case _ => complete(StatusCodes.InternalServerError)
                  }
                }
              }
            }
          } ~
            path("bidder") {
              post {
                entity(as[Bidder]) { bidder =>
                  val future = AuctionHouse.addBidder(auctionId, bidder)
                  onSuccess(future) {
                    case AddBidderResponseOKMessage(id) => complete(id.toString)
                    case AddBidderResponseKOMessage(message) => complete(StatusCodes.Forbidden, message)
                    case _ => complete(StatusCodes.InternalServerError)
                  }
                }
              }
            } ~
            pathEnd {
              val future = AuctionHouse.auction(auctionId)
              onSuccess(future) {
                case GetAuctionResponseMessage(presentAuctionValues) => complete(presentAuctionValues)
                case GetAuctionClosedResponseMessage(closedAuctionValues) => complete(closedAuctionValues)
                case AuctionNotFoundMessage() => complete(StatusCodes.NotFound, s"Auction $auctionId not found")
                case _ => complete(StatusCodes.InternalServerError)
              }
            }
        } ~
          path("auction") {
            post {
              entity(as[CreateAuctionDto]) {
                case CreateAuctionDto(auctioneer, auction) =>
                  val future = AuctionHouse.addAuction(auctioneer, auction)
                  onComplete(future) { done =>
                    complete(done.get.id.toString)
                  }
              }
            } ~
              put {
                entity(as[UpdateAuctionDto]) {
                  case UpdateAuctionDto(auctioneer, id, auction) =>
                    val future = AuctionHouse.updateAuction(auctioneer, id, auction)
                    onSuccess(future) {
                      case AuctionNotFoundMessage() => complete(StatusCodes.NotFound, s"Auction $id not found")
                      case UpdateAuctionResponseOKMessage() => complete(StatusCodes.OK)
                      case UpdateAuctionResponseKOMessage() => complete(StatusCodes.Forbidden)
                      case _ => complete(StatusCodes.InternalServerError)
                    }
                }
              }
          } ~
          get {
            complete("Hello world!")
          }
      }

    doStartServer(route)
  }

  private def doStartServer(route: Route) = {
    implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher

    val bindingFuture = Http().bindAndHandle(route, "localhost", port)
    val currentDirectory = new java.io.File(".").getCanonicalPath
    println(
      s"""Server online at http://localhost:$port/auctionHouse
      You can also test the application in another console
      with the bash script: $currentDirectory/src/test/resources/http-test/curl-test.sh
      That server works on foreground.
      Press RETURN to stop...""")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}
