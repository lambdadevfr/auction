package com.auction.dto

import java.time.ZonedDateTime

import com.auction.PublicId
import com.auction.model._

case class ClosedAuctionDto
(
  id: PublicId,
  auctionDefinition: Auction,
  bidderId: PublicId = null,
  winningBidder: Bidder = null,
  winningBid: Price = null,
  at: ZonedDateTime = null
)
