package com.auction.dto

import com.auction.model.{Auction, Auctioneer}

case class CreateAuctionDto
(
  val auctioneer: Auctioneer,

  val auction: Auction
)
