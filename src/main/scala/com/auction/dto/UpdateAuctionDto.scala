package com.auction.dto

import com.auction.PublicId
import com.auction.model.{Auction, Auctioneer}

case class UpdateAuctionDto
(
  val auctioneer: Auctioneer,

  val id: PublicId,

  val auction: Auction
)
