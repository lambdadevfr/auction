package com.auction.dto

import com.auction.PublicId
import com.auction.model.AuctionStatus.AuctionStatus
import com.auction.model._

case class PresentAuctionDto
(
  id: PublicId,
  auctionDefinition: Auction,
  currentPrice: Price,
  bidders: Map[PublicId, Bidder],
  bids: List[BidAccepted],
  status: AuctionStatus
)
