package com.auction

import com.auction.dto.{ClosedAuctionDto, PresentAuctionDto}
import com.auction.model._

package object actor {

  sealed trait AuctionHouseRequest

  sealed trait AuctionRequest

  sealed trait AuctionHouseResponse

  case class CreateAuctionMessage(auctioneer: Auctioneer, auctionDefinition: Auction) extends AuctionHouseRequest

  case class AuctionCreatedMessage(id: PublicId) extends AuctionHouseResponse

  case class UpdateAuctionMessage(auctioneer: Auctioneer, id: PublicId, auction: Auction) extends AuctionHouseRequest

  case class AuctionNotFoundMessage() extends AuctionHouseResponse

  // case class AuctionAck() extends AuctionHouseResponse


  case class AuctionsListMessage() extends AuctionHouseRequest

  case class AuctionsListResponseMessage(presentAuctions: List[PresentAuctionDto]) extends AuctionHouseResponse

  case class GetAuctionMessageById(id: PublicId) extends AuctionHouseRequest


  // Message re-routed to Auctions
  case class GetAuctionMessage() extends AuctionRequest

  case class UpdateAuction(auction: Auction) extends AuctionRequest

  case class AddBidderMessage(auctionId: PublicId, bidder: Bidder) extends AuctionRequest

  case class AddBidMessage(auctionId: PublicId, bidderId: PublicId, price: Price) extends AuctionRequest

  // Responses from Auction
  case class GetAuctionResponseMessage(presentAuctionValues: PresentAuctionDto) extends AuctionHouseResponse

  case class GetAuctionClosedResponseMessage(closedAuctionDto: ClosedAuctionDto) extends AuctionHouseResponse

  case class UpdateAuctionResponseOKMessage() extends AuctionHouseResponse

  case class UpdateAuctionResponseKOMessage() extends AuctionHouseResponse

  case class AddBidderResponseOKMessage(id: PublicId) extends AuctionHouseResponse

  case class AddBidderResponseKOMessage(message: String) extends AuctionHouseResponse

  case class AddBidResponseOKMessage() extends AuctionHouseResponse

  case class AddBidResponseKOMessage(message: String) extends AuctionHouseResponse

}
