package com.auction.actor

import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.pattern.ask
import com.auction.model._
import com.auction.{PublicId, timeout}

import scala.collection.mutable
import scala.concurrent.{Await, Future}


object AuctionHouse {
  val system = ActorSystem("AuctionHouseSystem")
  val auctionHouse = system.actorOf(Props[AuctionHouse], name = "auctionHouse")

  def addAuction(auctioneer: Auctioneer, auctionCreation: Auction): Future[AuctionCreatedMessage] =
    (auctionHouse ? CreateAuctionMessage(auctioneer, auctionCreation)).mapTo[AuctionCreatedMessage]

  def updateAuction(auctioneer: Auctioneer, id: PublicId, auctionDefinition: Auction) =
    (auctionHouse ? UpdateAuctionMessage(auctioneer, id, auctionDefinition)).mapTo[AuctionHouseResponse]

  def auctions() =
    (auctionHouse ? AuctionsListMessage()).mapTo[AuctionsListResponseMessage]


  def auction(id: PublicId) =
    (auctionHouse ? GetAuctionMessageById(id)).mapTo[AuctionHouseResponse]


  def addBidder(auctionId: PublicId, bidder: Bidder) =
    (auctionHouse ? AddBidderMessage(auctionId, bidder)).mapTo[AuctionHouseResponse]

  def addBid(auctionId: PublicId, bidderId: PublicId, price: Price) =
    (auctionHouse ? AddBidMessage(auctionId, bidderId, price)).mapTo[AuctionHouseResponse]

}


class AuctionHouse extends Actor with ActorLogging {
  var auctions = mutable.HashMap.empty[PublicId, ActorRef]

  def receive = {
    case CreateAuctionMessage(auctioneer, auction) =>
      val newId = newPublicId()

      val auctionActor = context.actorOf(Props(new AuctionActor(newId, auction)), name = s"auction-${newId}")
      auctions += (newId -> auctionActor)
      log.info(s"AuctionHouse received new Auction(from ${auctioneer}): ${newId} ${auction}")
      sender() ! AuctionCreatedMessage(newId)

    case UpdateAuctionMessage(auctioneer, id, auction) =>
      auctions.get(id) match {
        case Some(auctionRef) =>
          log.info(s"AuctionHouse received update (from ${auctioneer}) about ${id}: ${auction}")
          auctionRef forward UpdateAuction(auction)
        case None =>
          sender() ! AuctionNotFoundMessage()
      }

    case AuctionsListMessage() =>
      val presentAuctions = auctions.map {
        case (_, auctionActorRef) =>
          val future = (auctionActorRef ? GetAuctionMessage()).mapTo[GetAuctionResponseMessage]
          Await.result(future, timeout.duration).presentAuctionValues
      }.toList.sortBy(_.id)

      sender() ! AuctionsListResponseMessage(presentAuctions)

    case GetAuctionMessageById(auctionId: PublicId) =>
      auctions.get(auctionId) match {
        case Some(auctionRef) =>
          log.info(s"AuctionHouse found  auction ${auctionId}")
          auctionRef forward GetAuctionMessage()
        case None =>
          sender() ! AuctionNotFoundMessage()
      }

    case message@AddBidderMessage(auctionId, _) =>
      auctions.get(auctionId) match {
        case Some(auctionRef) =>
          log.info(s"AuctionHouse found  auction ${auctionId}")
          auctionRef forward message
        case None =>
          sender() ! AuctionNotFoundMessage()
      }

    case message@AddBidMessage(auctionId, _, _) =>
      auctions.get(auctionId) match {
        case Some(auctionRef) =>
          log.info(s"AuctionHouse found  auction ${auctionId}")
          auctionRef forward message
        case None =>
          sender() ! AuctionNotFoundMessage()
      }
  }

  private def newPublicId(): PublicId = UUID.randomUUID()
}
