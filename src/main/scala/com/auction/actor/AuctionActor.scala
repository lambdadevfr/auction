package com.auction.actor

import java.time.ZonedDateTime
import java.util.UUID

import akka.actor.{Actor, ActorLogging}
import com.auction.PublicId
import com.auction.dto.{ClosedAuctionDto, PresentAuctionDto}
import com.auction.model._


class AuctionActor(var id: PublicId, var auction: Auction)
  extends Actor with ActorLogging {

  var bids = List.empty[BidAccepted]
  var bidders = Map.empty[PublicId, Bidder]

  override def receive: Receive = {
    case GetAuctionMessage() =>
      val result = if (auctionStatus() == AuctionStatus.Closed) {
        if (bids.isEmpty) {
          GetAuctionClosedResponseMessage(
            ClosedAuctionDto(id, auction)
          )
        } else {
          val winner = bids.head
          GetAuctionClosedResponseMessage(
            ClosedAuctionDto(id, auction, winner.bidderId, winner.bidder, winner.price, winner.at)
          )
        }
      } else {
        GetAuctionResponseMessage(
          PresentAuctionDto(id, auction, currentPrice(), bidders, bids, auctionStatus())
        )
      }
      sender() ! result
    case UpdateAuction(newAuction: Auction) =>
      val result = if (auctionStatus() == AuctionStatus.Planned) {
        auction = newAuction
        UpdateAuctionResponseOKMessage()
      } else {
        UpdateAuctionResponseKOMessage()
      }
      sender() ! result

    case AddBidderMessage(_, bidder: Bidder) =>
      val value = auctionStatus()
      val result = if (value == AuctionStatus.Opened) {
        val newId = newPublicId()
        bidders += (newId -> bidder)
        log.info(s"Bidder $bidder added to auction $auction")
        AddBidderResponseOKMessage(newId)
      } else {
        AddBidderResponseKOMessage("That auction is not opened for bidders.")
      }
      sender() ! result

    case AddBidMessage(_, bidderId: PublicId, price: Price) =>
      val result =
        if (auctionStatus() == AuctionStatus.Opened) {
          bidders.get(bidderId) match {
            case Some(bidder: Bidder) =>
              if (auction.incrementPolicy.isBidValid(currentPrice(), price)) {
                bids = BidAccepted(bidderId, bidder, price) :: this.bids
                AddBidResponseOKMessage()
              } else {
                AddBidResponseKOMessage("That bid is not valid at that price $bid (policy: ${auction.incrementPolicy}).")
              }
            case None =>
              AddBidResponseKOMessage("That bidder is not registered for that auction.")
          }
        } else {
          AddBidResponseKOMessage("That auction is not opened for bids.")
        }

      sender() ! result
  }

  private def newPublicId(): PublicId = UUID.randomUUID()

  def currentPrice() = bids match {
    case last :: _ => last.price
    case _ => auction.initialPrice
  }

  def auctionStatus() = {
    val now = ZonedDateTime.now()
    if (now isBefore auction.beginAt) AuctionStatus.Planned
    else if (now isAfter auction.endAt) AuctionStatus.Closed
    else AuctionStatus.Opened
  }
}


