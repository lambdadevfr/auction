package com.auction.model

import java.time.ZonedDateTime

import com.auction.PublicId

case class BidAccepted(bidderId: PublicId, bidder: Bidder, price: Price, at: ZonedDateTime = ZonedDateTime.now())
