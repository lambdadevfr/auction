package com.auction.model

object AuctionStatus extends Enumeration {
  type AuctionStatus = Value
  val Planned, Opened, Closed = Value
}
