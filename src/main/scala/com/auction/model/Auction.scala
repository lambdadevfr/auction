package com.auction.model

import java.time.ZonedDateTime

case class Auction
(
  item: Item,
  initialPrice: Price,
  beginAt: ZonedDateTime,
  endAt: ZonedDateTime,
  // incrementPolicy: IncrementPolicy
  incrementPolicy: ConstantIncrementPolicy
) {
  require(beginAt.isBefore(endAt), "beginAt should be before endAt")
}
