package com.auction.model

case class Price(value: Long) {
  require(value > 0, "Price value must be positive")

  def add(anotherPrice: Price): Price = Price(this.value + anotherPrice.value)

  def isGreaterOrEquals(anotherPrice: Price): Boolean = this.value >= anotherPrice.value
}
