package com.auction.model

trait IncrementPolicy {
  def isBidValid(currentPrice: Price, newPrice: Price): Boolean
}

sealed case class ConstantIncrementPolicy(constant: Price) extends IncrementPolicy {
  override def isBidValid(currentPrice: Price, newPrice: Price): Boolean =
    newPrice.isGreaterOrEquals(currentPrice.add(constant))
}
