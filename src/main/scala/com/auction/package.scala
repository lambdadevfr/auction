package com

import java.util.UUID

import akka.util.Timeout

import scala.concurrent.duration._

package object auction {
  type PublicId = UUID
  implicit val timeout = Timeout(1 seconds)
}
