name := "Auction Project"

version := "1.0"

scalaVersion := "2.12.6"

lazy val akkaVersion = "2.5.19"
lazy val scalatestVersion = "3.0.5"

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

val akkaHttpVersion = "10.1.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "org.scalatest" %% "scalatest" % scalatestVersion % "test"
)
